#include <algorithm>
#include <iostream>
#include "NiceIterator.hpp"

using namespace iterators;

template <typename Iterator>
void iterate(Iterator& it) {
  auto iterator = it
    .filter([](auto x) { return isalpha(x); })
    .filter([](char x) { return x != 'a'; })
    .map<char>([](auto x) { return toupper(x); });

  auto count = iterator.count();
  /*while (true) {
    auto x = iterator.next();
    if (!x.has_value()) {
      break;
    }
    auto y = *x;
    std::cout << y;
  }*/
}

int main() {
  char str[] = "ana1abc";
  PointerNiceIterator<char> iterator(str, sizeof(str));
  iterate(iterator);
}