#pragma once

#include <optional>

namespace iterators {

using std::optional;

#define ADD_ITERATOR_FUNCTIONS                                                                                         \
  template <typename NewPredicate>                                                                                     \
  decltype(auto) filter(NewPredicate new_predicate) {                                                                  \
    return Filter<ThisClass, NewPredicate>(*this, new_predicate);                                                      \
  }                                                                                                                    \
                                                                                                                       \
  template <typename ReturnType, typename MapFunction>                                                                 \
  decltype(auto) map(MapFunction map_function) {                                                                       \
    return Map<ThisClass, ReturnType, MapFunction>(*this, map_function);                                               \
  }                                                                                                                    \
                                                                                                                       \
  template <typename ReturnType = size_t>                                                                              \
  ReturnType count() {                                                                                                 \
    ReturnType ret{};                                                                                                  \
    while (true) {                                                                                                     \
      auto value = next();                                                                                             \
      if (!value.has_value()) {                                                                                        \
        break;                                                                                                         \
      }                                                                                                                \
      ++ret;                                                                                                           \
    }                                                                                                                  \
    return ret;                                                                                                        \
  }

template <typename Iterator, typename Predicate>
class Filter;
template <typename Iterator, typename MapTo, typename MapFunction>
class Map;

template <typename T, typename Parent>
struct NiceIterator {
  using Item = T;

  optional<Item> next() { return static_cast<Parent&>(*this).next(); }
};

template <typename T>
class PointerNiceIterator {
public:
  using Item      = T;
  using ThisClass = PointerNiceIterator<T>;

  PointerNiceIterator(T* ptr, size_t size) : data(ptr), data_size(size) {}

  optional<Item> next() {
    if (data_size == 0) {
      return {};
    }
    auto ret = *data;
    ++data;
    --data_size;
    return ret;
  }

  ADD_ITERATOR_FUNCTIONS
private:
  T* data;
  size_t data_size;
};

template <typename Iterator, typename Predicate>
class Filter {
public:
  using Item      = typename Iterator::Item;
  using ThisClass = Filter<Iterator, Predicate>;

  Filter(Iterator iterator, Predicate predicate) : iterator(iterator), predicate(predicate) {}

  optional<Item> next() {
    while (true) {
      auto value = iterator.next();
      if (!value.has_value()) {
        return {};
      }
      if (predicate(*value)) {
        return value;
      }
    }
  }

  ADD_ITERATOR_FUNCTIONS
private:
  Iterator iterator;
  Predicate predicate;
};

template <typename Iterator, typename MapTo, typename MapFunction>
class Map {
public:
  using Item      = MapTo;
  using ThisClass = Map<Iterator, MapTo, MapFunction>;

  Map(Iterator iterator, MapFunction map_function) : iterator(iterator), map_function(map_function) {}

  optional<Item> next() {
    auto value = iterator.next();
    if (!value.has_value()) {
      return {};
    }
    return map_function(*value);
  }

  ADD_ITERATOR_FUNCTIONS
private:
  Iterator iterator;
  MapFunction map_function;
};

} // namespace iterators

#undef ADD_ITERATOR_FUNCTIONS